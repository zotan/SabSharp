﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net.Http;
using System.Web;
using Newtonsoft.Json;
using Vars;

// ReSharper disable UnusedMember.Global

namespace SabSharp
{
    public class SabClient
    {
        private readonly string _host;
        private readonly ushort _port;
        private readonly string _apikey;

        public SabClient(string host, ushort port, string apikey)
        {
            _host = host;
            _port = port;
            _apikey = apikey;
        }

        private string SendApiRequest(string getparams)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Add("User-Agent", "SabSharp");
            return client.GetStringAsync($"http://{_host}:{_port}/sabnzbd/api?output=json&apikey={_apikey}&{getparams}")
                .Result;
        }

        public SabQueueResponse GetQueue()
        {
            return SabQueueResponse.FromJson(SendApiRequest("mode=queue"));
        }

        public bool PauseQueue()
        {
            return SabBoolResponse.FromJson(SendApiRequest("mode=pause")).Status;
        }

        public bool PauseQueue(int minutes)
        {
            return SabBoolResponse.FromJson(SendApiRequest($"mode=config&name=set_pause&value={minutes}")).Status;
        }

        public bool ResumeQueue()
        {
            return SabBoolResponse.FromJson(SendApiRequest("mode=resume")).Status;
        }

        /// <summary>limit can either be of type "400K" (=> 400KB/s) or "30" (=> 30% of configured max. line speed)</summary>
        public bool SetSpeedlimit(string limit)
        {
            return SabBoolResponse.FromJson(SendApiRequest($"mode=config&name=speedlimit&value={limit}")).Status;
        }

        [Obsolete("Does not work for unknown reasons", true)]
        public bool SetCompleteAction(CompleteAction action)
        {
            return SabBoolResponse
                .FromJson(SendApiRequest(
                    $"mode=change_complete_action&value={Enum.GetName(typeof(CompleteAction), action)}")).Status;
        }

        /// <summary>script should be a filename (=> "email.py")</summary>
        [Obsolete("Does not work for unknown reasons", true)]
        public bool SetCompleteAction(string script)
        {
            return SabBoolResponse.FromJson(SendApiRequest($"mode=change_complete_action&value=script_{script}"))
                .Status;
        }

        public bool SortQueue(SortMethod method, SortDirection direction)
        {
            return SabBoolResponse
                .FromJson(SendApiRequest(
                    $"mode=queue&name=sort&sort={Enum.GetName(typeof(SortMethod), method)}&dir={Enum.GetName(typeof(SortDirection), direction)}desc"))
                .Status;
        }

        //TODO addfile + addlocalfile (does anybody actually use this)
        /// <summary>
        /// Adds an NZB file fetched from an URI to the queue
        /// </summary>
        /// <param name="nzbUri">Raw NZB file URI</param>
        /// <param name="nzbName">Name for the queue entry, can Include {{password}}</param>
        /// <param name="category">Category to be assigned, * means Default.</param>
        /// <param name="script">Script to be assigned</param>
        /// <param name="priority">-100: default priority; -2: paused; -1: low; 0: normal; 1: high; 2: force</param>
        /// <param name="postProcessParams">-1: default; 0: none; 1: +Repair; 2: +Repair/Unpack; 3: +Repair/Unpack/Delete</param>
        /// <returns></returns>
        public SabAddResponse AddToQueue(string nzbUri, string nzbName = "", string category = "*",
            string script = "Default",
            short priority = -100, short postProcessParams = -1)
        {
            return SabAddResponse.FromJson(SendApiRequest(
                $"mode=addurl&name={HttpUtility.UrlEncode(nzbUri)}&nzbname={nzbName}&cat={category}&script={script}" +
                $"&priority={priority}&pp={postProcessParams}"));
        }

        public bool PauseJob(string nzoId)
        {
            return SabBoolResponse.FromJson(SendApiRequest($"mode=queue&name=pause&value={nzoId}")).Status;
        }

        public bool ResumeJob(string nzoId)
        {
            return SabBoolResponse.FromJson(SendApiRequest($"mode=queue&name=resume&value={nzoId}")).Status;
        }

        public bool DeleteJob(string nzoId, bool deleteAlreadyDownloaded)
        {
            return deleteAlreadyDownloaded
                ? SabBoolResponse.FromJson(SendApiRequest($"mode=queue&name=delete&value={nzoId}&del_files=1")).Status
                : SabBoolResponse.FromJson(SendApiRequest($"mode=queue&name=delete&value={nzoId}")).Status;
        }

        public bool PurgeQueue(bool deleteAlreadyDownloaded, string onlyMatching = "")
        {
            return deleteAlreadyDownloaded
                ? SabBoolResponse.FromJson(SendApiRequest($"mode=queue&name=purge&search={onlyMatching}&del_files=1"))
                    .Status
                : SabBoolResponse.FromJson(SendApiRequest($"mode=queue&name=purge&search={onlyMatching}")).Status;
        }

        /// <summary>
        /// Switches nzoId1 with nzoId2
        /// </summary>
        public SabMoveEntryResponse SwitchJobs(string nzoId1, string nzoId2)
        {
            return SabMoveEntryResponse.FromJson(SendApiRequest($"mode=switch&value={nzoId1}&value2={nzoId2}"));
        }

        /// <summary>
        /// Moves nzoId1 to queue position newPosition
        /// </summary>
        /// <param name="nzoId1">nzoId of queue entry you want to move</param>
        /// <param name="newPosition">Queue position, with top starting at 0</param>
        public SabMoveEntryResponse SwitchJobs(string nzoId1, int newPosition)
        {
            return SabMoveEntryResponse.FromJson(SendApiRequest($"mode=switch&value={nzoId1}&value2={newPosition}"));
        }

        public bool ChangeJobCategory(string nzoId, string newCategory)
        {
            return SabBoolResponse.FromJson(SendApiRequest($"mode=change_cat&value={nzoId}&value2={newCategory}"))
                .Status;
        }

        public bool ChangeJobScript(string nzoId, string newScript)
        {
            return SabBoolResponse.FromJson(SendApiRequest($"mode=change_script&value={nzoId}&value2={newScript}"))
                .Status;
        }

        public bool ChangeJobPriority(string nzoId, int newPriority)
        {
            return SabBoolResponse
                .FromJson(SendApiRequest($"mode=queue&name=priority&value={nzoId}&value2={newPriority}")).Status;
        }

        public bool ChangeJobPpOpts(string nzoId, int newOptions)
        {
            return SabBoolResponse.FromJson(SendApiRequest($"mode=change_opts&value={nzoId}&value2={newOptions}"))
                .Status;
        }

        /// <summary>
        /// Changes a jobs nzbName, and possibly password, if included as {{pass}}
        /// </summary>
        public bool ChangeJobName(string nzoId, int newName)
        {
            return SabBoolResponse.FromJson(SendApiRequest($"mode=change_opts&value={nzoId}&value2={newName}")).Status;
        }

        public SabFilesResponse GetJobFiles(string nzoId)
        {
            return SabFilesResponse.FromJson(SendApiRequest($"mode=get_files&value={nzoId}"));
        }
        
        //TODO delete_nzf

        public SabHistoryResponse GetHistory(bool failedOnly = false, string search = "", string category = "",
            int limit = -1, int start = -1)
        {
            return SabHistoryResponse.FromJson(SendApiRequest(
                $"mode=history&start={(start == -1 ? "" : start.ToString())}&limit={(limit == -1 ? "" : limit.ToString())}&category={category}&search={search}&failed_only={(failedOnly ? 1 : 0)}"));
        }

        public SabBoolResponse RetryEntry(string nzoId, string password = "")
        {
            return SabBoolResponse.FromJson(SendApiRequest($"mode=retry&value={nzoId}&password={password}"));
        }

        public SabBoolResponse RetryAllEntries()
        {
            return SabBoolResponse.FromJson(SendApiRequest("mode=retry_all"));
        }

        public SabBoolResponse DeleteEntry(string nzoId, bool deleteFiles = false)
        {
            return SabBoolResponse.FromJson(
                SendApiRequest($"mode=history&name=delete&value={nzoId}&del_files={(deleteFiles ? 1 : 0)}"));
        }

        public SabBoolResponse DeleteEntries(IEnumerable<string> nzoIds, bool deleteFiles = false)
        {
            return SabBoolResponse.FromJson(
                SendApiRequest(
                    $"mode=history&name=delete&value={nzoIds.Aggregate("", (current, s) => current + s + ",").TrimEnd(',')}&del_files={(deleteFiles ? 1 : 0)}"));
        }

        public SabBoolResponse DeleteAllEntries(bool deleteFiles = false)
        {
            return SabBoolResponse.FromJson(
                SendApiRequest($"mode=history&name=delete&value=all&del_files={(deleteFiles ? 1 : 0)}"));
        }

        public SabBoolResponse DeleteFailedEntries(bool deleteFiles = false)
        {
            return SabBoolResponse.FromJson(
                SendApiRequest($"mode=history&name=delete&value=failed&del_files={(deleteFiles ? 1 : 0)}"));
        }

        public SabVersionResponse GetVersion()
        {
            return SabVersionResponse.FromJson(SendApiRequest("mode=version"));
        }

        //TODO auth

        public SabFullStatusResponse GetFullStatus(bool skipDashboard = false)
        {
            return SabFullStatusResponse.FromJson(
                SendApiRequest($"mode=fullstatus&skip_dashboard={(skipDashboard ? 1 : 0)}"));
        }

        public SabListCatrgoriesResponse GetCategories()
        {
            return SabListCatrgoriesResponse.FromJson(SendApiRequest("mode=get_cats"));
        }

        public SabListScriptsResponse GetScripts()
        {
            return SabListScriptsResponse.FromJson(SendApiRequest("mode=get_scripts"));
        }

        public SabDownloadStatsResponse GetDownloadStats()
        {
            return SabDownloadStatsResponse.FromJson(SendApiRequest("mode=server_stats"));
        }

        //TODO config (get_config, set_config, set_config_default, set_apikey, set_nzbkey, regenerate_certs)

        public SabWarningsResponse GetWarnings()
        {
            return SabWarningsResponse.FromJson(SendApiRequest("mode=warnings"));
        }

        public SabBoolResponse ClearWarnings()
        {
            return SabBoolResponse.FromJson(SendApiRequest("mode=warnings&name=clear"));
        }
        
        public SabBoolResponse ShutdownSab()
        {
            return SabBoolResponse.FromJson(SendApiRequest("mode=shutdown"));
        }
        
        public SabBoolResponse RestartSab()
        {
            return SabBoolResponse.FromJson(SendApiRequest("mode=restart"));
        }
        
        public SabBoolResponse PausePp()
        {
            return SabBoolResponse.FromJson(SendApiRequest("mode=pause_pp"));
        }
        
        public SabBoolResponse ResumePp()
        {
            return SabBoolResponse.FromJson(SendApiRequest("mode=resume_pp"));
        }
        
        public SabBoolResponse FetchRssNow()
        {
            return SabBoolResponse.FromJson(SendApiRequest("mode=rss_now"));
        }
        
        public SabBoolResponse ScanWatchedFolderNow()
        {
            return SabBoolResponse.FromJson(SendApiRequest("mode=watched_now"));
        }
        
        public SabBoolResponse ResetQuota()
        {
            return SabBoolResponse.FromJson(SendApiRequest("mode=reset_quote"));
        }

        //TODO translate
    }
}


namespace Vars
{
    public partial class SabAddResponse
    {
        [JsonProperty("status")]
        public bool Status { get; set; }

        [JsonProperty("nzo_ids")]
        public List<string> NzoIds { get; set; }
    }

    public partial class SabMoveEntryResponse
    {
        [JsonProperty("result")]
        public Result Result { get; set; }
    }

    public class Result
    {
        [JsonProperty("priority")]
        public long Priority { get; set; }

        [JsonProperty("position")]
        public long Position { get; set; }
    }

    public partial class SabMoveEntryResponse
    {
        public static SabMoveEntryResponse FromJson(string json) =>
            JsonConvert.DeserializeObject<SabMoveEntryResponse>(json, Converter.Settings);
    }

    public partial class SabAddResponse
    {
        public static SabAddResponse FromJson(string json) =>
            JsonConvert.DeserializeObject<SabAddResponse>(json, Converter.Settings);
    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum SortDirection
    {
        asc,
        desc
    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum SortMethod
    {
        avg_age,
        name,
        size
    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum CompleteAction
    {
        hibernate_pc,
        standby_pc,
        shutdown_program
    }

    public partial class SabBoolResponse
    {
        [JsonProperty("status")]
        public bool Status { get; set; }
    }

    public partial class SabBoolResponse
    {
        public static SabBoolResponse FromJson(string json) =>
            JsonConvert.DeserializeObject<SabBoolResponse>(json, Converter.Settings);
    }

    public partial class SabQueueResponse
    {
        [JsonProperty("queue")]
        public Queue Queue { get; set; }
    }

    public class Queue
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("speedlimit")]
        public string Speedlimit { get; set; }

        [JsonProperty("speedlimit_abs")]
        public string SpeedlimitAbs { get; set; }

        [JsonProperty("paused")]
        public bool Paused { get; set; }

        [JsonProperty("noofslots_total")]
        public long NoofslotsTotal { get; set; }

        [JsonProperty("noofslots")]
        public long Noofslots { get; set; }

        [JsonProperty("limit")]
        public long Limit { get; set; }

        [JsonProperty("start")]
        public long Start { get; set; }

        [JsonProperty("eta")]
        public string Eta { get; set; }

        [JsonProperty("timeleft")]
        public string Timeleft { get; set; }

        [JsonProperty("speed")]
        public string Speed { get; set; }

        [JsonProperty("kbpersec")]
        public string Kbpersec { get; set; }

        [JsonProperty("size")]
        public string Size { get; set; }

        [JsonProperty("sizeleft")]
        public string Sizeleft { get; set; }

        [JsonProperty("mb")]
        public string Mb { get; set; }

        [JsonProperty("mbleft")]
        public string Mbleft { get; set; }

        [JsonProperty("slots")]
        public List<Slot> Slots { get; set; }

        [JsonProperty("categories")]
        public List<string> Categories { get; set; }

        [JsonProperty("scripts")]
        public List<string> Scripts { get; set; }

        [JsonProperty("diskspace1")]
        public string Diskspace1 { get; set; }

        [JsonProperty("diskspace2")]
        public string Diskspace2 { get; set; }

        [JsonProperty("diskspacetotal1")]
        public string Diskspacetotal1 { get; set; }

        [JsonProperty("diskspacetotal2")]
        public string Diskspacetotal2 { get; set; }

        [JsonProperty("diskspace1_norm")]
        public string Diskspace1Norm { get; set; }

        [JsonProperty("diskspace2_norm")]
        public string Diskspace2Norm { get; set; }

        [JsonProperty("rating_enable")]
        public bool RatingEnable { get; set; }

        [JsonProperty("have_warnings")]
        public string HaveWarnings { get; set; }

        [JsonProperty("pause_int")]
        public string PauseInt { get; set; }

        [JsonProperty("loadavg")]
        public string Loadavg { get; set; }

        [JsonProperty("left_quota")]
        public string LeftQuota { get; set; }

        [JsonProperty("refresh_rate")]
        public string RefreshRate { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }

        [JsonProperty("finish")]
        public long Finish { get; set; }

        [JsonProperty("cache_art")]
        public string CacheArt { get; set; }

        [JsonProperty("cache_size")]
        public string CacheSize { get; set; }

        [JsonProperty("cache_max")]
        public string CacheMax { get; set; }

        [JsonProperty("finishaction")]
        public object Finishaction { get; set; }

        [JsonProperty("paused_all")]
        public bool PausedAll { get; set; }

        [JsonProperty("quota")]
        public string Quota { get; set; }

        [JsonProperty("have_quota")]
        public bool HaveQuota { get; set; }

        [JsonProperty("queue_details")]
        public string QueueDetails { get; set; }
    }

    public class Slot
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("index")]
        public long Index { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("avg_age")]
        public string AvgAge { get; set; }

        [JsonProperty("script")]
        public string Script { get; set; }

        [JsonProperty("has_rating")]
        public bool HasRating { get; set; }

        [JsonProperty("mb")]
        public string Mb { get; set; }

        [JsonProperty("mbleft")]
        public string Mbleft { get; set; }

        [JsonProperty("mbmissing")]
        public string Mbmissing { get; set; }

        [JsonProperty("size")]
        public string Size { get; set; }

        [JsonProperty("sizeleft")]
        public string Sizeleft { get; set; }

        [JsonProperty("filename")]
        public string Filename { get; set; }

        [JsonProperty("priority")]
        public string Priority { get; set; }

        [JsonProperty("cat")]
        public string Cat { get; set; }

        [JsonProperty("eta")]
        public string Eta { get; set; }

        [JsonProperty("timeleft")]
        public string Timeleft { get; set; }

        [JsonProperty("percentage")]
        public string Percentage { get; set; }

        [JsonProperty("nzo_id")]
        public string NzoId { get; set; }

        [JsonProperty("unpackopts")]
        public string Unpackopts { get; set; }
    }

    public partial class SabQueueResponse
    {
        public static SabQueueResponse FromJson(string json) =>
            JsonConvert.DeserializeObject<SabQueueResponse>(json, Converter.Settings);
    }

    public partial class SabFilesResponse
    {
        [JsonProperty("files")]
        public List<File> Files { get; set; }
    }

    public class File
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("mbleft")]
        public string Mbleft { get; set; }

        [JsonProperty("mb")]
        public string Mb { get; set; }

        [JsonProperty("age")]
        public string Age { get; set; }

        [JsonProperty("bytes")]
        public string Bytes { get; set; }

        [JsonProperty("filename")]
        public string Filename { get; set; }

        [JsonProperty("nzf_id")]
        public string NzfId { get; set; }

        [JsonProperty("set")]
        public string Set { get; set; }
    }

    public partial class SabFilesResponse
    {
        public static SabFilesResponse FromJson(string json) =>
            JsonConvert.DeserializeObject<SabFilesResponse>(json, Converter.Settings);
    }

    public partial class SabHistoryResponse
    {
        [JsonProperty("history")]
        public History History { get; set; }
    }

    public class History
    {
        [JsonProperty("noofslots")]
        public long Noofslots { get; set; }

        [JsonProperty("day_size")]
        public string DaySize { get; set; }

        [JsonProperty("week_size")]
        public string WeekSize { get; set; }

        [JsonProperty("month_size")]
        public string MonthSize { get; set; }

        [JsonProperty("total_size")]
        public string TotalSize { get; set; }

        [JsonProperty("last_history_update")]
        public long LastHistoryUpdate { get; set; }

        [JsonProperty("slots")]
        public List<HistorySlot> Slots { get; set; }
    }

    public class HistorySlot
    {
        [JsonProperty("action_line")]
        public string ActionLine { get; set; }

        [JsonProperty("series")]
        public string Series { get; set; }

        [JsonProperty("show_details")]
        public string ShowDetails { get; set; }

        [JsonProperty("script_log")]
        public string ScriptLog { get; set; }

        [JsonProperty("meta")]
        public object Meta { get; set; }

        [JsonProperty("fail_message")]
        public string FailMessage { get; set; }

        [JsonProperty("loaded")]
        public bool Loaded { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("size")]
        public string Size { get; set; }

        [JsonProperty("category")]
        public string Category { get; set; }

        [JsonProperty("pp")]
        public string Pp { get; set; }

        [JsonProperty("retry")]
        public long Retry { get; set; }

        [JsonProperty("completeness")]
        public long Completeness { get; set; }

        [JsonProperty("script")]
        public string Script { get; set; }

        [JsonProperty("nzb_name")]
        public string NzbName { get; set; }

        [JsonProperty("download_time")]
        public long DownloadTime { get; set; }

        [JsonProperty("storage")]
        public string Storage { get; set; }

        [JsonProperty("has_rating")]
        public bool HasRating { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("script_line")]
        public string ScriptLine { get; set; }

        [JsonProperty("completed")]
        public long Completed { get; set; }

        [JsonProperty("nzo_id")]
        public string NzoId { get; set; }

        [JsonProperty("downloaded")]
        public long Downloaded { get; set; }

        [JsonProperty("report")]
        public string Report { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("path")]
        public string Path { get; set; }

        [JsonProperty("postproc_time")]
        public long PostprocTime { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("md5sum")]
        public string Md5Sum { get; set; }

        [JsonProperty("bytes")]
        public long Bytes { get; set; }

        [JsonProperty("url_info")]
        public string UrlInfo { get; set; }

        [JsonProperty("stage_log")]
        public List<StageLog> StageLog { get; set; }
    }

    public class StageLog
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("actions")]
        public List<string> Actions { get; set; }
    }

    public partial class SabHistoryResponse
    {
        public static SabHistoryResponse FromJson(string json) =>
            JsonConvert.DeserializeObject<SabHistoryResponse>(json, Converter.Settings);
    }

    public partial class SabVersionResponse
    {
        [JsonProperty("version")]
        public string Version { get; set; }
    }

    public partial class SabVersionResponse
    {
        public static SabVersionResponse FromJson(string json) =>
            JsonConvert.DeserializeObject<SabVersionResponse>(json, Converter.Settings);
    }

    public partial class SabFullStatusResponse
    {
        [JsonProperty("status")]
        public Status Status { get; set; }
    }

    public class Status
    {
        [JsonProperty("localipv4")]
        public string Localipv4 { get; set; }

        [JsonProperty("ipv6")]
        public object Ipv6 { get; set; }

        [JsonProperty("publicipv4")]
        public string Publicipv4 { get; set; }

        [JsonProperty("dnslookup")]
        public string Dnslookup { get; set; }

        [JsonProperty("folders")]
        public List<string> Folders { get; set; }

        [JsonProperty("cpumodel")]
        public string Cpumodel { get; set; }

        [JsonProperty("pystone")]
        public long Pystone { get; set; }

        [JsonProperty("downloaddir")]
        public string Downloaddir { get; set; }

        [JsonProperty("downloaddirspeed")]
        public long Downloaddirspeed { get; set; }

        [JsonProperty("completedir")]
        public string Completedir { get; set; }

        [JsonProperty("completedirspeed")]
        public long Completedirspeed { get; set; }

        [JsonProperty("loglevel")]
        public string Loglevel { get; set; }

        [JsonProperty("logfile")]
        public string Logfile { get; set; }

        [JsonProperty("configfn")]
        public string Configfn { get; set; }

        [JsonProperty("nt")]
        public bool Nt { get; set; }

        [JsonProperty("darwin")]
        public bool Darwin { get; set; }

        [JsonProperty("helpuri")]
        public string Helpuri { get; set; }

        [JsonProperty("uptime")]
        public string Uptime { get; set; }

        [JsonProperty("color_scheme")]
        public string ColorScheme { get; set; }

        [JsonProperty("webdir")]
        public string Webdir { get; set; }

        [JsonProperty("active_lang")]
        public string ActiveLang { get; set; }

        [JsonProperty("restart_req")]
        public bool RestartReq { get; set; }

        [JsonProperty("power_options")]
        public bool PowerOptions { get; set; }

        [JsonProperty("pp_pause_event")]
        public bool PpPauseEvent { get; set; }

        [JsonProperty("pid")]
        public long Pid { get; set; }

        [JsonProperty("weblogfile")]
        public object Weblogfile { get; set; }

        [JsonProperty("new_rel_url")]
        public object NewRelUrl { get; set; }

        [JsonProperty("have_warnings")]
        public string HaveWarnings { get; set; }

        [JsonProperty("warnings")]
        public List<object> Warnings { get; set; }

        [JsonProperty("servers")]
        public List<Server> Servers { get; set; }
    }

    public class Server
    {
        [JsonProperty("servername")]
        public string Servername { get; set; }

        [JsonProperty("servertotalconn")]
        public long Servertotalconn { get; set; }

        [JsonProperty("serverssl")]
        public long Serverssl { get; set; }

        [JsonProperty("serveractiveconn")]
        public long Serveractiveconn { get; set; }

        [JsonProperty("serveroptional")]
        public long Serveroptional { get; set; }

        [JsonProperty("serveractive")]
        public bool Serveractive { get; set; }

        [JsonProperty("servererror")]
        public string Servererror { get; set; }

        [JsonProperty("serverpriority")]
        public long Serverpriority { get; set; }

        [JsonProperty("serverconnections")]
        public List<Serverconnection> Serverconnections { get; set; }
    }

    public class Serverconnection
    {
        [JsonProperty("thrdnum")]
        public long Thrdnum { get; set; }

        [JsonProperty("nzo_name")]
        public string NzoName { get; set; }

        [JsonProperty("nzf_name")]
        public string NzfName { get; set; }

        [JsonProperty("art_name")]
        public string ArtName { get; set; }
    }

    public partial class SabListCatrgoriesResponse
    {
        [JsonProperty("categories")]
        public List<string> Categories { get; set; }
    }

    public partial class SabListCatrgoriesResponse
    {
        public static SabListCatrgoriesResponse FromJson(string json) =>
            JsonConvert.DeserializeObject<SabListCatrgoriesResponse>(json, Converter.Settings);
    }

    public partial class SabFullStatusResponse
    {
        public static SabFullStatusResponse FromJson(string json) =>
            JsonConvert.DeserializeObject<SabFullStatusResponse>(json, Converter.Settings);
    }

    public partial class SabListScriptsResponse
    {
        [JsonProperty("scripts")]
        public List<string> Scripts { get; set; }
    }

    public partial class SabListScriptsResponse
    {
        public static SabListScriptsResponse FromJson(string json) =>
            JsonConvert.DeserializeObject<SabListScriptsResponse>(json, Converter.Settings);
    }

    public partial class SabDownloadStatsResponse
    {
        [JsonProperty("day")]
        public long Day { get; set; }

        [JsonProperty("week")]
        public long Week { get; set; }

        [JsonProperty("month")]
        public long Month { get; set; }

        [JsonProperty("total")]
        public long Total { get; set; }

        [JsonProperty("servers")]
        public Servers Servers { get; set; }

        [JsonProperty("daily")]
        public Daily Daily { get; set; }
    }

    public class Servers
    {
        [JsonProperty("eunews.server.com")]
        public SabDownloadStatsResponse EunewsServerCom { get; set; }

        [JsonProperty("News.server.net")]
        public SabDownloadStatsResponse NewsServerNet { get; set; }
    }

    public class Daily
    {
        [JsonProperty("2017-01-28")]
        public long The20170128 { get; set; }

        [JsonProperty("2017-01-29")]
        public long The20170129 { get; set; }
    }

    public partial class SabDownloadStatsResponse
    {
        public static SabDownloadStatsResponse FromJson(string json) =>
            JsonConvert.DeserializeObject<SabDownloadStatsResponse>(json, Converter.Settings);
    }

    public partial class SabWarningsResponse
    {
        [JsonProperty("warnings")]
        public List<Warning> Warnings { get; set; }
    }

    public class Warning
    {
        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("time")]
        public long Time { get; set; }
    }

    public partial class SabWarningsResponse
    {
        public static SabWarningsResponse FromJson(string json) =>
            JsonConvert.DeserializeObject<SabWarningsResponse>(json, Converter.Settings);
    }

    public static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}